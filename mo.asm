; nasm include file for macOS

; System calls used in `.asm`
sys_write EQU 0x2000004
sys_exit EQU 0x2000001

; expands to entire SECTION directive required
%define SECTION_DICT_RWX SECTION __DICT,__data

%macro SAVE_IPL 0
       mov rbp, aiplsave
       mov rdx, [rsp]
       mov [rbp], rdx
       lea rcx, [rsp+8]
       mov [rbp+8], rcx
       add rdx, 1
       shl rdx, 3
       add rcx, rdx
       mov [rbp+16], rcx
%endmacro

; expands to OS specific rc.4 FORTH file
%define OSRC 'rc10mo.4'
