; nasm include file for Linux

sys_write EQU 1
sys_exit EQU 60

%define SECTION_DICT_RWX SECTION .data exec

%macro SAVE_IPL 0
       mov rbp, aiplsave
       mov rdx, [rsp]
       mov [rbp], rdx
       lea rcx, [rsp+8]
       mov [rbp+8], rcx
       add rdx, 1
       shl rdx, 3
       add rcx, rdx
       mov [rbp+16], rcx
%endmacro

%define OSRC 'rc10li.4'
