#include <stdio.h>

#include <termios.h>
#include <sys/ioctl.h>

int main(void)
{
    // This is the simplest way to find out the value of these constants,
    // which effectively form part of the macOS ABI.
    printf("TIOCGETA %lx\n", TIOCGETA);
    printf("TIOCSETA %lx\n", TIOCSETA);
    printf("TIOCGWINSZ %lx\n", TIOCGWINSZ);
}
