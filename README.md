# SixtyForth

[![Build Status](https://travis-ci.org/drj11/sixtyforth.svg?branch=master)](https://travis-ci.org/drj11/sixtyforth)

Welcome to SixtyForth, `64th` for short.

SixtyForth is a 64-bit implementation of FORTH
written in 64-bit Intel assembler.
macOS is the primary operating system, but
also developed for Linux.

SixtyForth implements the ANSI Forth standard (1994 edition).
The CORE word set is present.
The most practical reference is the
[Forth Standard website](https://forth-standard.org/).

In this implementation a cell is 64-bits wide,
and signed integers are represented in 2's complement.

As per the ANSI standard,
the required words from the standard are required to be in UPPER CASE.
Obviously lower case is a bit more comfortable,
and SixtyForth may accept that in the future.
Note that you can define your own words in any case you like;
when using them the case must be exactly as you defined it.

Input is accepted from the keyboard
(and interactively
`Ctrl A` `Ctrl E` `Ctrl P` `Ctrl N` `Ctrl D` `Ctrl K` work);
from the command line (`./64th -c 'SOURCE TYPE'`);
and from files (`./64th example/hw.4`).

## Building SixtyForth and running it

As of 2021 building is supported for 64-bit Intel macOS and
64-bit Intel Linux.

Unpack the distribution from the magnetic tape.
Ensure NASM is installed.

A shell script must be executed, there is one for each platform.
On macOS:

    cd sixtyforth
    sh mk.mox6na

On Linux:

    cd sixtyforth
    sh mk.lix6na

This creates the executable binary `64th`.
Run this to start using SixtyForth:

    ./64th


## Platform names

The name of the build script encodes 3-levels of platform abstraction,
a combination of Operating System, CPU Architecture, and
Compilation Toolchain.
In principle this is extended to other platforms:

    mk.mox6na
       | | XX------------------ compilation toolchain (na = NASM)
       | XX---------------- CPU architecture (x6 = x86-64 / AMD64)
       XX-------------- operating system (mo = macOS, li = Linux)


## macOS

To install build tools.

    brew install nasm
    brew install binutils

`brew` advises:

If you need to have binutils first in your PATH, run:

    echo 'export PATH="/usr/local/opt/binutils/bin:$PATH"' >> ~/.zshrc

For compilers to find binutils you may need to set:

    export LDFLAGS="-L/usr/local/opt/binutils/lib"
    export CPPFLAGS="-I/usr/local/opt/binutils/include"


## Testing

The skeptical may want to run the tests first:

    make test

The tests are run using `urchin` which is an `npm` module.
`urchin` will be installed if `npm` works.

Or install [`fixrun`](https://gitlab.com/drj11/fixrun) and use that.


## Bugs, Issues, Fun

Please refer to gitlab.

https://gitlab.com/drj11/sixtyforth/


## On ANSI

The CORE word set is implemented.

Other word sets are not implemented completely,
but the intention is that where SixtyForth
has facilities that overlap with ANSI words,
SixtyForth will implement those ANSI words.

For example,
SixtyForth has a word that
adds two double cell numbers together,
it is `D+` from the ANSI DOUBLE word set,
even though the entire word set is not implemented.


### CORE EXT

None of the words marked as obsolescent are implemented:
`#TIB` `CONVERT` `EXPECT` `QUERY` `SPAN` `TIB`.

Implemented:

- `.(`
- `0>` `2>R` `2R>` `2R@`
- `<>` `?DO`
- `COMPILE,`
- `FALSE` `HEX`
- `NIP`
- `PAD` `PARSE`
- `TO` `TRUE` `TUCK`
- `U>`
- `VALUE` `WITHIN` `[COMPILE]` `\`

Not implemented:

- `.R` `0<>`
- `:NONAME`
- `AGAIN` `C"` `CASE`
- `ENDCASE` `ENDOF` `ERASE`
- `MARKER`
- `OF`
- `PICK` `REFILL` `RESTORE-INPUT` `ROLL` `SAVE-INPUT` `SOURCE-ID`
- `U.R`
- `UNUSED`


## DOUBLE

The following words from the DOUBLE word set are implemented:

- `M+`
- `D>S`
- `2ROT`
- `2VARIABLE`
- `D0=`
- `D+`
- `DNEGATE`
- `DABS`
- `D.`


# END
