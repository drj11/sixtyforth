# Notes about macOS

## Differences and similarities from Linux

- :syscall:numbers:
- :nasm:section:
- :rc:include:
- :text:sentinel:
- :jmp:rel:
- :argc:reg:
- :segment:prot:
- :build:
- :termios:struct:
- :fstat:struct:


:syscall:numbers the system call numbers are different, and
the flag values are different, even where the syscall implements
the same POSIX API.
On the upside, often the behaviour of the syscall itself is the
same or similar enough.

:nasm:section section names in NASM are different.

:rc:include On Linux rc.4 was included via an objcopy directive.
This seems not to be possible on macOS, mostly due to what is a
bug in objcopy when using the Mach-O object format.
A more portable (but more tedious) solution was found:
create an `.asm` file that includes the `rc.4` file.

:text:sentinel for no particularly good reason, as far as i can
tell, the text segment (or possible section) must start with a
symbol.

:jmp:rel in `doestarget` `jmp [.n+0]` was changed to
`jmp [rel .n]`, but i can no longer tell if that's required or not.

:argc:reg on Linux argc, argv are in memory;
on macOS the three actual arguments argc, argv, and envp are
passed in registers, following the AMD64 ABI: rdi, rsi, rdx.
On macOS those values need saving so that the `rc.4` code,
which runs _much_ later, can access them.
This general scheme, of saving those values,
could be implemented on Linux too.

Plot twist: if you link with `-static` you don't need `-lSystem`
and _then_ the argc and argv are laid out like Linux:
$rsp has address of argc and argv follows in memory.

:segment:prot on macOS the default protections for the data
segment (__DATA in Mach-O) are not executable.
So the implementation of `DOES>` fails.
Currently the macOS implementation fixes this by placing the
dictionary (and quite possibly some unrelated data) in a new
segment, `__DICT`, and marking this segment as `rwx` with a
linker option.
Should i ever contemplate a move to Applie Silicon,
this won't work.
We would need to `mmap()` a memory area and mark it `MAP_JIT`.

:build the build steps are different.

:termios:struct the termios struct is different.

:fstat:struct is the fstat struct different?


## Chaotic notes

so far:

objcopy and nasm require different architectures.

macOS uses Mach-O binary executable file format.
Mach-O has more restrictions on section names.

`objcopy` can't create Mach-O from rc.4 file and create the right
symbols that `ld` can link against. Workaround: a new .asm file
that includes rc.4.

Calling convention.
Should be same as Linux (see [IDRYMAN2014]).
Add 0x2000000 to all syscall numbers.

syscall numbers in [BSDSYSCALLS].

Mach-O .rodata section cannot be executed (therefore cannot
contain assembler instructions).
I think I had always been a little bit uneasy about this.

Think the executable section issue is also causing problems for
`CREATE DOES>`, because that places code in the data segment.

On macOS the data segments are not executable.
On 2021 Apple Silicon it is not possible to a segment that is
writable and executable (W^X).
See [APPLEJIT].
Maybe on Intel it is still possible to make a segment that is
writable and executable?

The Mach-O executable has initprot and maxprot fields in the
segment header to set the protections.

Can inspect with

    otool -h 64th

`size` gives some information, but does not include the loader
commands that `otool` shows:

    xcrun size -c -l -m 64th


## ld segprot

`ld -segprot __DATA rwx rwx` sets the maxprot and initprot but
the resulting executable still doesn't run:

; ./64th
dyld: malformed mach-o image: segment __DATA has vmsize != filesize and is executable
zsh: abort      ./64th

But, I can create a segment with the correct requirements;
in the `.asm` file:

    SECTION __DICT,__data

In the linker line:

    ln -segprot __DICT rwx rwx


## Initial register state, argv

Some experimentation with LLDB and an optimistic reading of the
ABI suggests that at initial program load the registers reflect
a standard ABI call:
- $rdi : argc
- $rsi : argv
- $rdx : envp

Unfortunately, SixtyForth destroys rdi, rsi, rdx;
so i added code to _start to save them in the new `iplsave`
3-word variable.


## tty modes

I was working through rc20post.4 to check that the
implementation of things looked plausible when i noticed that
`ki.n @ .` reports `8`.
This raised the hypothesis: Is the keyboard editor working, it's
just that the terminal isn't updated?
To which the answer is, sort of yes.
Inputting 1 2 3 ^A 4 [enter] results in `4123`,
exactly as if the editor is working and ^A moved the cursor to
the beginning of the line.


## REFERENCES

[APPLEJIT]
http://web.archive.org/save/https://developer.apple.com/documentation/apple-silicon/porting-just-in-time-compilers-to-apple-silicon

[IDRYMAN2014]
# Quite useful, includes references on syscalls and function
# call calling convention (which FORTH doesn't use).
http://www.idryman.org/blog/2014/12/02/writing-64-bit-assembly-on-mac-os-x/

[BSDSYSCALLS]
https://sigsegv.pl/osx-bsd-syscalls/

# END
