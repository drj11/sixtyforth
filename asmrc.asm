BITS 64

extern rc_begin
extern rc_size

SECTION .rodata

rc_begin:

INCBIN 'rc00boot.4'
INCBIN OSRC
INCBIN 'rc20post.4'

rc_size EQU $ - rc_begin
